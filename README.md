# README #



### What is this repository for? ###

Use this template to create your own noteblog: a blog for Jupyter notebooks.

#### Quick summary

1. [Fork this repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
2. Get a local clone of the fork on your system
3. Obtain and install an [Anaconda python distribution](https://www.continuum.io/downloads)
4. Create a new, bare Python 2.7 conda environment: `conda create -n noteblog python=2.7`
4. Activate the new environment: `source activate noteblog`
5. Install the Python pre-requisites: `pip install -r requirements.txt`
6. Customize the `pelicanconf.py` file (particularly the `AUTHOR` and `SITENAME` fields)
7. Copy new Jupyter notebooks to the `incoming_notebooks` directory: e.g., `cp -p ./*.ipynb`
7. If you have never used git before: `git config --global user.email "you@example.com" && git config --global user.name "Your Name"`
8. Add these as noteblog entries: `./scripts/addIncoming.bash`
9. (Optional) start a simple webserver: `make serve` and view the NoteBlog in your browser: [http://localhost:8000](http://localhost:8000)