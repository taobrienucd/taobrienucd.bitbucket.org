from numpy import *

def genBoundingBoxes(lat,lon,latIndSize=5,lonIndSize=5):
    """Takes a lat/lon mesh and splits it up into chunks and returns the bounding boxes of the chunks"""

    latEdges = lat[::latIndSize]
    lonEdges = lon[::lonIndSize]

    latEdges = concatenate((latEdges,[lat[-1]]))
    lonEdges = concatenate((lonEdges,[lon[-1]]))

    lonE,latE = meshgrid(lonEdges,latEdges)

    lowerLats = latE[:-1,:-1]
    upperLats = latE[1:,1:]

    lowerLons = lonE[:-1,:-1]
    upperLons = lonE[1:,1:]

    nbounds = len(lowerLats.ravel())

    boundingBoxes = [ \
                        [   lowerLats.ravel()[i],   \
                            upperLats.ravel()[i],   \
                            lowerLons.ravel()[i],   \
                            upperLons.ravel()[i]] for i in range(nbounds) ]

    return boundingBoxes


if __name__ == "__main__":

    import netCDF4 as nc
    with nc.Dataset("regridded.iliad-ne120np4-2005.2deg.precip.nc","r") as fin:
        lat = fin.variables["lat"][:]
        lon = fin.variables["lon"][:]

    bboxes = genBoundingBoxes(lat,lon,5,5)
    for box in bboxes:
        print(box)
