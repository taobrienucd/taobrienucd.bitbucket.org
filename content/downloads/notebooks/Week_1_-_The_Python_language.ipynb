{
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  },
  "name": ""
 },
 "nbformat": 3,
 "nbformat_minor": 0,
 "worksheets": [
  {
   "cells": [
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "# Objectives\n",
      "* Have thorough grasp of basic Python and programming concepts\n",
      "* Know where to go to learn more\n",
      "* Have the scientific method be fundamentally integrated into programming approach"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "# Basic programming concepts\n",
      "\n",
      "## General approach to writing code\n",
      "\n",
      "The following is the approach to programming that I will attempt to convey throughout the quarter:\n",
      "\n",
      "1. Start with a *pseudocode* version of your algorithm; consider physically sketching it out on paper\n",
      "2. Refine the algorithm to be as modular as possible (e.g., write a separate function for reading in data and a separate function for doing data analysis)\n",
      "3. Write the pseudocode algorithm as a series of code comments (your code will then be pre-commented!)\n",
      "4. Start filling in the actual code that satisfies the algorithm; test and debug individual modules (functions) as you proceed\n",
      "5. When encountering unexpected behavior, use the scientific method to debug:\n",
      "    * Develop a hypothesis about what could be causing the bug\n",
      "        * Read any error messages (in particular, look for Python calling out specific lines that are causing a problem)\n",
      "        * Use debugging tools (or cleverly-placed print statements) to examine the state of the code: is it what you expect?\n",
      "        * Generate plots to visualize intermediate results; compare to what you expect\n",
      "    * Come up with ways to falsify the hypothesis\n",
      "    * Attempt to falsify the hypothesis; if falsified, then start over; if not, then proceed\n",
      "    * Fix the bug (this may be the same as attempting to falsify the hypothesis for simple problems)\n",
      "6. Comment your code thoroughly!!! (~1 comment per line of code)\n",
      "\n",
      "\n",
      "## Important Python syntax and rules\n",
      "    \n",
      "### Indentation controls everything\n",
      "In Python, **indentation controls the flow of the code**.  Consider the two following simple for-loops, which have very different behavior:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" loop 1a\"\"\"\n",
      "a = []\n",
      "for i in range(10):\n",
      "    a.append(i)\n",
      "print('a:',a)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "a: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]\n"
       ]
      }
     ],
     "prompt_number": 1
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" loop 1b\"\"\"\n",
      "a = []\n",
      "for i in range(10):\n",
      "    a.append(i)\n",
      "    print('a:',a)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "a: [0]\n",
        "a: [0, 1]\n",
        "a: [0, 1, 2]\n",
        "a: [0, 1, 2, 3]\n",
        "a: [0, 1, 2, 3, 4]\n",
        "a: [0, 1, 2, 3, 4, 5]\n",
        "a: [0, 1, 2, 3, 4, 5, 6]\n",
        "a: [0, 1, 2, 3, 4, 5, 6, 7]\n",
        "a: [0, 1, 2, 3, 4, 5, 6, 7, 8]\n",
        "a: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]\n"
       ]
      }
     ],
     "prompt_number": 2
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "What is the difference between these two code blocks?  Why does one only print once and the other prints 10 times?\n",
      "\n",
      "This indentation rule is true for all sorts of Python code: for loops, while loops, function definitions, class definitions, try/except blocks, etc.  Indentation controls whether code belongs within a given code block.\n",
      "\n",
      "### Functions and indentation\n",
      "\n",
      "However, there is an important subtlety to this rule when it comes to functions.  Consider the following two functions:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": true,
     "input": [
      "\"\"\" Function 1a \"\"\"\n",
      "def add_one_and_print(a):\n",
      "    a = a + 1\n",
      "    return\n",
      "    print(a)\n",
      "    \n",
      "add_one_and_print(1)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [],
     "prompt_number": 3
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" Function 1b \"\"\"\n",
      "def add_one_and_print(a):\n",
      "    a = a + 1\n",
      "    print(a)\n",
      "    return\n",
      "    \n",
      "add_one_and_print(1)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "2\n"
       ]
      }
     ],
     "prompt_number": 4
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "What differs between these two functions?  Why does one print something and another doesn't?  In the upper one, the `return` statement means that Python leaves the function immediately upon encountering. So the code can never actually reach the `print a` statement, even though `print a` is technically part of `add_one_and_print()` because its indentation matches the indentation of the rest of the code within the function.\n",
      "\n",
      "### Functions and variable definitions (scope)\n",
      "In Python, variables defined within a function are visible only within the code of that function; they aren't visible elsewhere.  The following codeblock gives an error becuase I attempt to get at a variable that is *out of scope* (meaning it isn't defined in the current portion of the code)."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" out-of-scope demo \"\"\"\n",
      "# define a function\n",
      "def function_scope_demo():\n",
      "    c = 1\n",
      "    print(c)\n",
      "    return\n",
      "\n",
      "# run the function\n",
      "function_scope_demo()\n",
      "\n",
      "# attempt to print c again\n",
      "print(c)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "1\n"
       ]
      },
      {
       "ename": "NameError",
       "evalue": "name 'c' is not defined",
       "output_type": "pyerr",
       "traceback": [
        "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
        "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
        "\u001b[0;32m<ipython-input-5-25a50b023672>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     10\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     11\u001b[0m \u001b[0;31m# attempt to print c again\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 12\u001b[0;31m \u001b[0mprint\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mc\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
        "\u001b[0;31mNameError\u001b[0m: name 'c' is not defined"
       ]
      }
     ],
     "prompt_number": 5
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "However, functions *do* have (read) access to variables that are already defined at the time the function is called."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" closure demo 1\"\"\"\n",
      "c = \"Hello\"\n",
      "def re_define_c():\n",
      "    print(\"within function, c =\",c)\n",
      "    return\n",
      "\n",
      "re_define_c()\n",
      "print(\"outside function, c =\",c)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "within function, c = Hello\n",
        "outside function, c = Hello\n"
       ]
      }
     ],
     "prompt_number": 6
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "But note that you can't overwrite a variable from an *outer scope* within a function.  In the following example, I add a simple `c = 1` at the top of the function; notice that `c` remains unchanged after the function is called."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" closure demo 2 \"\"\"\n",
      "c = \"Hello\"\n",
      "def re_define_c():\n",
      "    c = 1\n",
      "    print(\"within function, c =\",c)\n",
      "    return\n",
      "\n",
      "re_define_c()\n",
      "print(\"outside function, c =\",c)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "within function, c = 1\n",
        "outside function, c = Hello\n"
       ]
      }
     ],
     "prompt_number": 7
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Function return values (position matters!)\n",
      "Essentially the only way to get data out of a function (with a few exceptions) is to return values from within the function:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" return demo 1 \"\"\"\n",
      "c = \"Hello\"\n",
      "def re_define_c():\n",
      "    c = 1\n",
      "    print(\"within function, c =\",c)\n",
      "    return c\n",
      "\n",
      "# re-define c as the result of the function\n",
      "c = re_define_c()\n",
      "print(\"outside function, c =\",c)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "within function, c = 1\n",
        "outside function, c = 1\n"
       ]
      }
     ],
     "prompt_number": 8
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "And remember, if you want to return multiple variables, you can specfiy multiple values with a comma.  But remember that the position of return values is critical (their name is not)."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" return demo 2a \"\"\"\n",
      "def return_two_vars():\n",
      "    xxxyff = 1\n",
      "    oiawjef = 2\n",
      "    return xxxyff, oiawjef\n",
      "\n",
      "a, b = return_two_vars()\n",
      "\n",
      "print('a = ',a, ', b = ', b)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "a =  1 , b =  2\n"
       ]
      }
     ],
     "prompt_number": 9
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" return demo 2b \"\"\"\n",
      "def return_two_vars():\n",
      "    xxxyff = 1\n",
      "    oiawjef = 2\n",
      "    return oiawjef, xxxyff\n",
      "\n",
      "a, b = return_two_vars()\n",
      "\n",
      "print('a = ',a, ', b = ', b)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "a =  2 , b =  1\n"
       ]
      }
     ],
     "prompt_number": 10
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "What is the difference between these two functions?  The code is essentially identical, but in one, a=1, and in the other a=2."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Objects and types\n",
      "\n",
      "In python **everything** is an object.  There are a few 'fundamendal' object types on which most everything is built:\n",
      "\n",
      " * strings (`str`)\n",
      " * numbers (`int`, `float`)\n",
      " * iterables (`tuple`, `list`, `dict`)\n",
      " * functions\n",
      " * classes (for defining new objects as combinations of these types)\n",
      " \n",
      "\n",
      "### String basics\n",
      "\n",
      "Strings are human-readable sets of characters, and they have a variety of useful methods associated with them:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "# define a string\n",
      "a = \"I'm going to learn a ton in ATM 298!\"\n",
      "# print the string\n",
      "print('a = ',a)\n",
      "\n",
      "# show that a is a string\n",
      "print('type(a) = ',type(a))"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "a =  I'm going to learn a ton in ATM 298!\n",
        "type(a) =  <class 'str'>\n"
       ]
      }
     ],
     "prompt_number": 11
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "# strings can be added to other strings\n",
      "b = a + \" (j/k)\"\n",
      "print('b = ',b)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "b =  I'm going to learn a ton in ATM 298! (j/k)\n"
       ]
      }
     ],
     "prompt_number": 12
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "# the string format method method can add text to a placeholder (empty curly brackets)\n",
      "# Add curly brackets to the end of the string\n",
      "c = b + \" {}\"\n",
      "print(c.format(\"(j/k about the first j/k)\"))"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "I'm going to learn a ton in ATM 298! (j/k) (j/k about the first j/k)\n"
       ]
      }
     ],
     "prompt_number": 13
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "# strings can be split into lists using a specific character as a delimiter (whitespace is the default delimiter)\n",
      "d = c.split()\n",
      "print('type(d) = ',type(d))\n",
      "print('d = ',d)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "type(d) =  <class 'list'>\n",
        "d =  [\"I'm\", 'going', 'to', 'learn', 'a', 'ton', 'in', 'ATM', '298!', '(j/k)', '{}']\n"
       ]
      }
     ],
     "prompt_number": 14
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "(Why are there empty curly brackets in the end of the `d` list? Look back at the cell above for a hint.)"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Number basics\n",
      "\n",
      "Python numbers are the most basic type, and you can do all the obvious things with them: addition, subtraction, division, multiplication."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "# define two numbers and add them\n",
      "a = 1\n",
      "b = 2\n",
      "print('type(a) = ',type(a))\n",
      "print('a = {}, b = {}, a + b = {}'.format(a,b,a+b))"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "type(a) =  <class 'int'>\n",
        "a = 1, b = 2, a + b = 3\n"
       ]
      }
     ],
     "prompt_number": 15
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "(Note the use of the string `format()` method above with three arguments.  The position of the argument to `format()` corresponds to the position of the empty curly brackets in the string.)"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "# define two numbers and divide them\n",
      "c = 1.0\n",
      "d = 3\n",
      "print('type(c) = ',type(c))\n",
      "\n",
      "# print the dividing numbers\n",
      "print('c = {}, d = {}, c/d = {}'.format(c,d,c/d))\n",
      "\n",
      "# print again using number formatting\n",
      "print('c = {:0.0f}, d = {}, c/d = {:3.3f}'.format(c,d,c/d))"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "type(c) =  <class 'float'>\n",
        "c = 1.0, d = 3, c/d = 0.3333333333333333\n",
        "c = 1, d = 3, c/d = 0.333\n"
       ]
      }
     ],
     "prompt_number": 16
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Note the use of the `:0.0f` and `:3.3f` within the first and last curly bracket.  This causes the `format()` method to print the first number with 0 decimal places, and the last number with only three decimal places."
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Iterable basics\n",
      "\n",
      "### Tuples\n",
      "The most basic iterable is a tuple, which can be defined in one of two (nearly) equivalent ways:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" tuple demo \"\"\"\n",
      "# define a tuple using parentheses ()\n",
      "a = (1,3,5,7)\n",
      "# define a tuple using tuple() (and pass in a list)\n",
      "b = tuple([1,3,5,7])\n",
      "\n",
      "print('type(a) = ',type(a))\n",
      "print(a)\n",
      "\n",
      "print('type(b) = ',type(b))\n",
      "print(b)\n",
      "\n",
      "# check if they are equivalent\n",
      "print('Are a and b equivalent? ',a == b)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "type(a) =  <class 'tuple'>\n",
        "(1, 3, 5, 7)\n",
        "type(b) =  <class 'tuple'>\n",
        "(1, 3, 5, 7)\n",
        "Are a and b equivalent?  True\n"
       ]
      }
     ],
     "prompt_number": 17
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "All iterable objects in Python can be looped over within a `for` loop:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" Iterating over a tuple \"\"\"\n",
      "# print the items in the tuple 'a'\n",
      "for item in a:\n",
      "    print(item)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "1\n",
        "3\n",
        "5\n",
        "7\n"
       ]
      }
     ],
     "prompt_number": 18
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Lists\n",
      "\n",
      "Lists differ from tuples in that they are *mutable*: this means that they can be modified (items can be changed, the list can be added to, etc.)  In contrast, `tuple` objects are *immutable*, meaning that they cannot be modified.  In the following code block, and error is raised because I try to modify a specific item within a tuple:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" immutable tuples \"\"\"\n",
      "a[0] = -11"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "ename": "TypeError",
       "evalue": "'tuple' object does not support item assignment",
       "output_type": "pyerr",
       "traceback": [
        "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
        "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
        "\u001b[0;32m<ipython-input-19-dc044749216e>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0;34m\"\"\" immutable tuples \"\"\"\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 2\u001b[0;31m \u001b[0ma\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;36m0\u001b[0m\u001b[0;34m]\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;34m-\u001b[0m\u001b[0;36m11\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
        "\u001b[0;31mTypeError\u001b[0m: 'tuple' object does not support item assignment"
       ]
      }
     ],
     "prompt_number": 19
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "However, this does work for lists.  I repeat the code above using a list instead (a list defined from the tuple):"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" mutable lists \"\"\"\n",
      "b = list(a)\n",
      "b[0] = \"nothing\"\n",
      "print('a = ',a)\n",
      "print('b = ',b)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "a =  (1, 3, 5, 7)\n",
        "b =  ['nothing', 3, 5, 7]\n"
       ]
      }
     ],
     "prompt_number": 20
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "(Notice that you can mix types within a list or tuple: `b` has items that are both strings and integers.)\n",
      "\n",
      "By the way, the `range()` function simply creates a list from a given number:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "a = range(10)\n",
      "print('type(a) = ',type(a))\n",
      "print(a)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "type(a) =  <class 'range'>\n",
        "range(0, 10)\n"
       ]
      }
     ],
     "prompt_number": 21
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Which is why you often see `range()` used in the notation for basic `for` loops:\n",
      "\n",
      "```python\n",
      "for i in range(10):\n",
      "    # do stuff with i\n",
      "```"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Items within `tuples` and `lists` can be *indexed* using numbers. Values at specific locations can be referenced using single integers, or using slice notation."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" indexing specific values \"\"\"\n",
      "a = range(10)\n",
      "# get the 2nd value within a\n",
      "c = a[1]\n",
      "# get the last value within a\n",
      "d = a[-1]\n",
      "\n",
      "print('type(a) = ',type(a))\n",
      "print('a = ',a)\n",
      "\n",
      "print('type(c) = ',type(c))\n",
      "print('c = a[1] = ',c)\n",
      "\n",
      "print('type(d) = ',type(d))\n",
      "print('d = a[-1] = ',d)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "type(a) =  <class 'range'>\n",
        "a =  range(0, 10)\n",
        "type(c) =  <class 'int'>\n",
        "c = a[1] =  1\n",
        "type(d) =  <class 'int'>\n",
        "d = a[-1] =  9\n"
       ]
      }
     ],
     "prompt_number": 22
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" indexing lists using slices \"\"\"\n",
      "a = range(10)\n",
      "# set b to be equal to the 4th, 5th, and 6th value from a (the slice 3:6 does this)\n",
      "b = a[3:6]\n",
      "\n",
      "print('type(a) = ',type(a))\n",
      "print('a = ',a)\n",
      "\n",
      "print('type(b) = ',type(b))\n",
      "print('b = ',b)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "type(a) =  <class 'range'>\n",
        "a =  range(0, 10)\n",
        "type(b) =  <class 'range'>\n",
        "b =  range(3, 6)\n"
       ]
      }
     ],
     "prompt_number": 23
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "And note that lists can be added to using the `append()` function:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "# create an empty list\n",
      "a = list()\n",
      "\n",
      "# loop from 0 to 9\n",
      "for i in range(10):\n",
      "    # append the current number to the list\n",
      "    a.append(i)\n",
      "    \n",
      "# print the results\n",
      "print('type(a) = ',type(a))\n",
      "print('a = ',a)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "type(a) =  <class 'list'>\n",
        "a =  [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]\n"
       ]
      }
     ],
     "prompt_number": 24
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "### Dictionaries\n",
      "\n",
      "Python dictionaries are kind of like lists, except you reference locations using strings (or other objects)."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" Dictionary demo \"\"\"\n",
      "# initialize an empty dictionary (could also do `a = {}`)\n",
      "a = dict()\n",
      "\n",
      "# insert items into a\n",
      "a['blue'] = 1\n",
      "a['swaying trees'] = \"Hello\"\n",
      "a[1] = 1.5\n",
      "\n",
      "# print the results\n",
      "print('type(a) = ',type(a))\n",
      "print('a = ',a)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "type(a) =  <class 'dict'>\n",
        "a =  {'blue': 1, 'swaying trees': 'Hello', 1: 1.5}\n"
       ]
      }
     ],
     "prompt_number": 25
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "They are also iterables.  Iteration occurs over *keys* (things like 'blue' above):"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "\"\"\" iterating over a dictionary \"\"\"\n",
      "for item in a:\n",
      "    print(item, ':', a[item])"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "blue : 1\n",
        "swaying trees : Hello\n",
        "1 : 1.5\n"
       ]
      }
     ],
     "prompt_number": 26
    },
    {
     "cell_type": "heading",
     "level": 1,
     "metadata": {},
     "source": [
      "Learning more"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "As you continue to use Python, you'll undoubtedly want to learn more.  There are three main sources of information that might be useful:\n",
      "\n",
      " 1. Python documentation (e.g., type `help(int)`)\n",
      " 2. The internet (e.g., google \"python list comprehensions\")\n",
      " 3. Trial and error (the scientific method)\n",
      " 3. Other Python-knowledgable people (e.g., Travis)"
     ]
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## Internal documentation\n",
      "Recall that most well-devleoped Python code is self-documented.  For example, the following code block prints documentation for the numpy `masked_where()` function, which tells you how it should be used:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import numpy\n",
      "help(numpy.ma.masked_where)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "Help on function masked_where in module numpy.ma.core:\n",
        "\n",
        "masked_where(condition, a, copy=True)\n",
        "    Mask an array where a condition is met.\n",
        "    \n",
        "    Return `a` as an array masked where `condition` is True.\n",
        "    Any masked values of `a` or `condition` are also masked in the output.\n",
        "    \n",
        "    Parameters\n",
        "    ----------\n",
        "    condition : array_like\n",
        "        Masking condition.  When `condition` tests floating point values for\n",
        "        equality, consider using ``masked_values`` instead.\n",
        "    a : array_like\n",
        "        Array to mask.\n",
        "    copy : bool\n",
        "        If True (default) make a copy of `a` in the result.  If False modify\n",
        "        `a` in place and return a view.\n",
        "    \n",
        "    Returns\n",
        "    -------\n",
        "    result : MaskedArray\n",
        "        The result of masking `a` where `condition` is True.\n",
        "    \n",
        "    See Also\n",
        "    --------\n",
        "    masked_values : Mask using floating point equality.\n",
        "    masked_equal : Mask where equal to a given value.\n",
        "    masked_not_equal : Mask where `not` equal to a given value.\n",
        "    masked_less_equal : Mask where less than or equal to a given value.\n",
        "    masked_greater_equal : Mask where greater than or equal to a given value.\n",
        "    masked_less : Mask where less than a given value.\n",
        "    masked_greater : Mask where greater than a given value.\n",
        "    masked_inside : Mask inside a given interval.\n",
        "    masked_outside : Mask outside a given interval.\n",
        "    masked_invalid : Mask invalid values (NaNs or infs).\n",
        "    \n",
        "    Examples\n",
        "    --------\n",
        "    >>> import numpy.ma as ma\n",
        "    >>> a = np.arange(4)\n",
        "    >>> a\n",
        "    array([0, 1, 2, 3])\n",
        "    >>> ma.masked_where(a <= 2, a)\n",
        "    masked_array(data = [-- -- -- 3],\n",
        "          mask = [ True  True  True False],\n",
        "          fill_value=999999)\n",
        "    \n",
        "    Mask array `b` conditional on `a`.\n",
        "    \n",
        "    >>> b = ['a', 'b', 'c', 'd']\n",
        "    >>> ma.masked_where(a == 2, b)\n",
        "    masked_array(data = [a b -- d],\n",
        "          mask = [False False  True False],\n",
        "          fill_value=N/A)\n",
        "    \n",
        "    Effect of the `copy` argument.\n",
        "    \n",
        "    >>> c = ma.masked_where(a <= 2, a)\n",
        "    >>> c\n",
        "    masked_array(data = [-- -- -- 3],\n",
        "          mask = [ True  True  True False],\n",
        "          fill_value=999999)\n",
        "    >>> c[0] = 99\n",
        "    >>> c\n",
        "    masked_array(data = [99 -- -- 3],\n",
        "          mask = [False  True  True False],\n",
        "          fill_value=999999)\n",
        "    >>> a\n",
        "    array([0, 1, 2, 3])\n",
        "    >>> c = ma.masked_where(a <= 2, a, copy=False)\n",
        "    >>> c[0] = 99\n",
        "    >>> c\n",
        "    masked_array(data = [99 -- -- 3],\n",
        "          mask = [False  True  True False],\n",
        "          fill_value=999999)\n",
        "    >>> a\n",
        "    array([99,  1,  2,  3])\n",
        "    \n",
        "    When `condition` or `a` contain masked values.\n",
        "    \n",
        "    >>> a = np.arange(4)\n",
        "    >>> a = ma.masked_where(a == 2, a)\n",
        "    >>> a\n",
        "    masked_array(data = [0 1 -- 3],\n",
        "          mask = [False False  True False],\n",
        "          fill_value=999999)\n",
        "    >>> b = np.arange(4)\n",
        "    >>> b = ma.masked_where(b == 0, b)\n",
        "    >>> b\n",
        "    masked_array(data = [-- 1 2 3],\n",
        "          mask = [ True False False False],\n",
        "          fill_value=999999)\n",
        "    >>> ma.masked_where(a == 3, b)\n",
        "    masked_array(data = [-- 1 -- --],\n",
        "          mask = [ True False  True  True],\n",
        "          fill_value=999999)\n",
        "\n"
       ]
      }
     ],
     "prompt_number": 27
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "## The internet\n",
      "\n",
      "You can truly master Python using information from the internet.  You all have ample experience, from class, using the internet to learn about specific aspects of Python.\n",
      "\n",
      "## Trial and error\n",
      "\n",
      "Sometimes you might think you have an idea how to do something in Python: try it!!  There is no better way to learn.  Remeber to apply the scientific method systematically: come up with one or more hypotheses about how Python works, attempt to falsify it (them), repeat (or proceed).\n",
      "\n",
      "## People\n",
      "\n",
      "And finally, there are plenty of Python-knowledgeable people out there.  I'll always be a resource to help point you in the right direction: feel free to e-mail me."
     ]
    }
   ],
   "metadata": {}
  }
 ]
}
