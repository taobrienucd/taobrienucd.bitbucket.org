{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Objectives:\n",
    "\n",
    " * Learn how to plot with multiple panels\n",
    " * Learn how to add a unified colorbar to plots\n",
    " * Learn techniques for generating publication qualitfy figures\n",
    " \n",
    "This lesson duplicates Week 5 Lesson 2, but using `cartopy` instead of `basemap` for geospatial plotting.  I do this partly because I realized as Python and matplotlib have advanced, `basemap` has not been updated in 3 years and makes use of matplotlib and python features that are no longer well-supported, which leads to unexpected (and hard to track down) bugs.\n",
    "\n",
    "Note that I am new to `cartopy`, so there may be better ways to do some of this."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook explores using advanced plotting techniques to plot geospatial data.  It involves use of the `cartopy` module, which can be installed by running `conda install -c scitools cartopy` from the Anaconda Prompt (or the command line).\n",
    "\n",
    "In this specific example, I demonstrate plotting a publication-quality 4-panel figure showing a snapshot of precipitation from an observation dataset (PERSIANN) and 3 climate model hindcasts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "\"\"\"Load necessary libraries\"\"\"\n",
    "# import the sys library for setting where scripts can import from\n",
    "import sys\n",
    "\n",
    "# add the netCDF library\n",
    "import netCDF4 as nc\n",
    "\n",
    "# import plotting libraries\n",
    "import pylab as PP\n",
    "import matplotlib as mpl\n",
    "\n",
    "# import a custom library for generating a grid of boxes from which to select data; used here to generate\n",
    "# polygons for drawing.\n",
    "# this can be downloaded at https://taobrienucd.bitbucket.io/class_scripts/genBoundingBoxes.py\n",
    "sys.path.append('../class_scripts') # allow importing from the class_scripts folder\n",
    "import genBoundingBoxes as gen\n",
    "\n",
    "# import cartopy (use `conda install -s scitools cartopy` to install)\n",
    "import cartopy.crs as ccrs\n",
    "\n",
    "# import numpy\n",
    "from numpy import *\n",
    "\n",
    "# use inline plotting\n",
    "%matplotlib inline\n",
    "\n",
    "# set default fonts\n",
    "font = { 'family' : 'serif', \\\n",
    "        'size' : '15' }\n",
    "mpl.rc('font', **font)\n",
    "mpl.rc('axes', labelweight = 'bold') # needed for bold axis labels in more recent version of matplotlib\n",
    "\n",
    "\n",
    "# Define a helper function for lat/lon boxes on a projection\n",
    "def bboxToPolygon(bbox,m,shiftLons = True):\n",
    "    \"\"\"Converts a bounding box from genBoundingBoxes into x,y,width,height suitable for mpl.patches.Polygon\n",
    "    \n",
    "       input:\n",
    "       ------\n",
    "       \n",
    "       bbox : a tuple of lat/lon indices describing the segements of a polygon\n",
    "       \n",
    "       m : a Basemap instance\n",
    "       \n",
    "       shiftLons : flags whether to shift longitudes by -180 degrees\n",
    "       \n",
    "       output:\n",
    "       -------\n",
    "       a list of polygon coordinates suitable for mpl.patches.Polygon()\n",
    "    \n",
    "    \"\"\"\n",
    "\n",
    "    lat0,lat1,lon0,lon1 = bbox\n",
    "    # shift longitudes\n",
    "    if shiftLons:\n",
    "        lon0 -= 180\n",
    "        lon1 -= 180\n",
    "\n",
    "    # Generate the list of polygon segments\n",
    "    segments = [((lat0,lon0),(lat1,lon0)), \\\n",
    "                ((lat1,lon0),(lat1,lon1)), \\\n",
    "                ((lat1,lon1),(lat0,lon1)), \\\n",
    "                ((lat0,lon1),(lat0,lon0))]\n",
    "\n",
    "\n",
    "    # use a list comprehension to do the mapping quickly\n",
    "    # see below for a description of what this does\n",
    "    seg_res = 10 # set the segment resolution\n",
    "    xy = concatenate([ m(linspace(seg[0][1],seg[1][1],seg_res),linspace(seg[0][0],seg[1][0],seg_res)) for seg in segments],axis = 1).T\n",
    "    # the above list comprehension is equivalent to the below code:\n",
    "    #    ysegs = []\n",
    "    #    xsegs = []\n",
    "    #    # loop over each segment and generate an higher-res segement interpolated to map space\n",
    "    #\n",
    "    #    for seg in segments:\n",
    "    #        # use the basemap instance to map a line segment from lat/lon coordinates to projection coordinates\n",
    "    #        xs,ys = m(linspace(seg[0][1],seg[1][1],seg_res),linspace(seg[0][0],seg[1][0],seg_res))\n",
    "    #        if xsegs == []:\n",
    "    #            xsegs = xs\n",
    "    #        else:\n",
    "    #            xsegs = concatenate((xsegs,xs))\n",
    "    #        if ysegs == []:\n",
    "    #            ysegs = xs\n",
    "    #        else:\n",
    "    #            ysegs = concatenate((ysegs,ys))\n",
    "    #\n",
    "    #    #stich the segments together into an array appropriate for Polygon\n",
    "    #    xy = array([array(xsegs),array(ysegs)]).T\n",
    "    \n",
    "    return xy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "\"\"\" Read precip fields into a dictionary\"\"\"\n",
    "\n",
    "itime = 0\n",
    "precip = {}\n",
    "# set the resolutions\n",
    "resolutions = ['ne16np4','ne30np4','ne120np4']\n",
    "# set file names (these files can be downloaded from):\n",
    "#  https://taobrienucd.bitbucket.io/data/persiann_precip_itime_1821.nc\n",
    "#  https://taobrienucd.bitbucket.io/data/iliad_precip_ne120np4_itime_1821.nc\n",
    "#  https://taobrienucd.bitbucket.io/data/iliad_precip_ne30np4_itime_1821.nc\n",
    "#  https://taobrienucd.bitbucket.io/data/iliad_precip_ne16np4_itime_1821.nc\n",
    "iliad_file_template = '../data/iliad_precip_{res}_itime_1821.nc'\n",
    "persiann_file = '../data/persiann_precip_itime_1821.nc'\n",
    "\n",
    "#*****************\n",
    "# Read in fields \n",
    "#*****************\n",
    "# read in the PERSIANN field\n",
    "with nc.Dataset(persiann_file,'r') as fin:\n",
    "    precip_persiann = fin.variables['precipitation'][0,...]\n",
    "    lat = fin.variables['lat'][:]\n",
    "    lon = fin.variables['lon'][:]\n",
    "\n",
    "# store the precip field\n",
    "precip['PERSIANN'] = precip_persiann\n",
    "    \n",
    "# loop over resolutions\n",
    "for res in resolutions:\n",
    "    # set the iliad file name\n",
    "    iliad_file = iliad_file_template.format(res=res)\n",
    "    \n",
    "    # read in the ILIAD field\n",
    "    with nc.Dataset(iliad_file,'r') as fin:\n",
    "        precip_iliad = fin.variables['PRECT'][0,...]\n",
    "        \n",
    "    # store in the dictionary and convert to mm/day\n",
    "    precip[res] = precip_iliad*8.64e7\n",
    "    #zero-out negative values that resulted from the interpolation\n",
    "    i_less_zero = where(precip[res] < 0)\n",
    "    precip[res][i_less_zero] = 0.0\n",
    "\n",
    "# produce 2D lat/lon arrays\n",
    "xlat,xlon = meshgrid(lat,lon)\n",
    "xlat = ascontiguousarray(xlat.T)\n",
    "xlon = ascontiguousarray(xlon.T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "\"\"\" Get the bounding boxes used by the pairedEvents routine \"\"\"\n",
    "latBoxSize = 20\n",
    "lonBoxSize = 20\n",
    "resolution = 2\n",
    "#Generate bounding boxes\n",
    "latIndSize = int(latBoxSize/resolution)\n",
    "lonIndSize = int(lonBoxSize/resolution)\n",
    "bboxes = gen.genBoundingBoxes(lat,lon,latIndSize,lonIndSize)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "module 'cartopy.crs' has no attribute 'Robinson'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-7-a3a3395b0e37>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     23\u001b[0m     \u001b[0;31m# center the longitude at the mid-point of the map\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     24\u001b[0m     \u001b[0mcenter_lon\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;36m0.5\u001b[0m\u001b[0;34m*\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mlon\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;36m0\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m+\u001b[0m\u001b[0mlon\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m-\u001b[0m\u001b[0;36m1\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 25\u001b[0;31m     \u001b[0mprojection\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mcartopy\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mcrs\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mRobinson\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mcenter_lon\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     26\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     27\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mAttributeError\u001b[0m: module 'cartopy.crs' has no attribute 'Robinson'"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAA7UAAAHbCAYAAAAUOL02AAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAIABJREFUeJzt3X+s5XV9J/7ny0H5MWzjjx1FKtQW6LpIV1nH2oJmXSis\nQlO7ok26JC1tI6TrFhPWVKPGJdVg1oRspCVuJ8vXTXYT3bQam+0yFlFwLWOUQWzApA3s1iUypY7t\nZitTpGV8ff84n8NcLnfuPfece2fuZ+7jkdwcPu9zXue+z5tzefE853Pep7o7AAAAMEbPOd4TAAAA\ngHkJtQAAAIyWUAsAAMBoCbUAAACMllALAADAaAm1AAAAjNa6Qm1V/UpV/VVV3Tjj7Z9fVZ+oqvuq\n6t6q+lRVvWSumQIAz6I3A7DdzRRqq+oFVfVHSS5M8sJ13P+nk+xMsru7X5vke0n2VtWOdc8UAHia\n3gwAE7O+U7szyY3dff2sd1xVlya5JMmHu7uH4Q9l0nyvWtcsAYDl9GYAyIyhtru/3d1fWed9X5Hk\nUJIHltzPI0keHa4DAOakNwPAxGZuFHVukseWvBI89WiS8zbx9wIAK9ObATjhnLSJ9316kidXGH8y\nyQ+tVFBV1ya5Nkl27tz5mle84hWbNzsAtpX77rvvu92963jP4zjTmwHYMjaqN29mqH08yckrjJ+c\nyalPz9Lde5LsSZLdu3f3/v37N292AGwrVfV/jvcctgC9GYAtY6N682aefvxwkjOqqpaNn5nkoU38\nvQDAyvRmAE44mxlqb89kZ8YLpgNVdVaSlyXZu4m/FwBYmd4MwAlnQ0JtVZ1SVQ9U1W3Tse7+QpK7\nkrx/ySvCH0hyf5Lf34jfCwCsTG8GYLuYOdRW1e9V1d3D4TVVdXdV/dz06iSnJTl1WdlVSb6fZH9V\n3Zvk+Une3N2HF5s2AKA3A8A6Norq7revct0TSc5ZYfz/JrlmrpkBAKvSmwFgcz9TCwAAAJtKqAUA\nAGC0hFoAAABGS6gFAABgtIRaAAAARkuoBQAAYLSEWgAAAEZLqAUAAGC0hFoAAABGS6gFAABgtIRa\nAAAARkuoBQAAYLSEWgAAAEZLqAUAAGC0hFoAAABGS6gFAABgtIRaAAAARkuoBQAAYLSEWgAAAEZL\nqAUAAGC0hFoAAABGS6gFAABgtIRaAAAARkuoBQAAYLSEWgAAAEZLqAUAAGC0hFoAAABGS6gFAABg\ntGYKtVV1flXdWVX3VNX9VXVTVZ00Q91lVfXlqto3XN5RVa9ZfNoAsL3pzQAwsWaorapdSe5K8tnu\nvjjJ65NcmeTmNerOTfKHSf6guy/q7jckuSPJHcN9AgBz0JsB4IhZ3qm9Pkkl+XiSdPehTJrmO6vq\nzFXqXpXkeUn2Lhnbm+SFSS6ea7YAQKI3A8DTZgm1VyTZ392Hl4ztS7IjyeWr1H0pyYEk11TVc6rq\nOUmuGa77iznmCgBM6M0AMFjzszdJzk1y/7KxR4fL845W1N3fraqLk/zXTBroD5K8KMlHuvurK9VU\n1bVJrk2Ss88+e4apAcC2pDcDwGCWd2pPT/LksrHp8c6jFQ2nP30xyX1JXjb8vDXJnxytprv3dPfu\n7t69a5eP9gDAUejNADCY5Z3ax5OcvGxsenxolbp3JzkjyXu7+6kkqarPJXm0qnZ29/+33skCAEn0\nZgB42izv1D6cZPmmE9Pjh1ape0WSA939xHRg+OzPt5L80jrmCAA8k94MAINZQu3tSXZX1Y4lYxcl\nOZzJ1wAczSNJXrysLpk03b9d1ywBgKX0ZgAYzBJqb0nSSa5Lkqo6LckNSW7t7gPD2ClV9UBV3bak\nbk+SU5P82+lAVf1akrOS/JeNmT4AbEt6MwAM1vxMbXcfrKpLktxSVVdnsgHF3iQfXHKzSnJaJo1y\nWvf1qrosyY1V9S+H4R1Jru7uT27UAwCA7UZvBoAjZtkoKt39zSSXrnL9E0nOWWH87iRvnHNuAMBR\n6M0AMDHL6ccAAACwJQm1AAAAjJZQCwAAwGgJtQAAAIyWUAsAAMBoCbUAAACMllALAADAaAm1AAAA\njJZQCwAAwGgJtQAAAIyWUAsAAMBoCbUAAACMllALAADAaAm1AAAAjJZQCwAAwGgJtQAAAIyWUAsA\nAMBoCbUAAACMllALAADAaAm1AAAAjJZQCwAAwGgJtQAAAIyWUAsAAMBoCbUAAACMllALAADAaAm1\nAAAAjJZQCwAAwGjNFGqr6vyqurOq7qmq+6vqpqo6acbad1TVl6rqrqr6s+HygsWmDQDbm94MABNr\nNr+q2pXkriQf6u7fqaqdSfYl2ZnkXWvU/laS1yf52e7+XlX9gyT3Jnl5kgcXnDsAbEt6MwAcMcs7\ntdcnqSQfT5LuPpTk5iTvrKozj1ZUVeckeV+S67v7e0Pt95K8LcnXFpw3AGxnejMADGYJtVck2d/d\nh5eM7UuyI8nlq9S9PcnB7n7Gq77d/WB3f2fdMwUApvRmABjMEmrPTXJg2dijw+V5q9S9Osm3q+qX\nq+qLVbWvqj5TVa+bZ6IAwNP0ZgAYzBJqT0/y5LKx6fHOVepelORVSa5MckV3X5TkS0n2VdVFKxVU\n1bVVtb+q9h88eHCGqQHAtqQ3A8BgllD7eJKTl41Njw+tUvdUkucmeX93fz9JuvtjSb6V5D0rFXT3\nnu7e3d27d+3aNcPUAGBb0psBYDBLqH04yfJNJ6bHD61S98iyy6k/z+qnRgEAq9ObAWAwS6i9Pcnu\nqtqxZOyiJIeT3LFK3ReGyx9eNv7SJI/NPEMAYDm9GQAGs4TaW5J0kuuSpKpOS3JDklu7+8AwdkpV\nPVBVty2p+0ySP0ny3qqq4XZvSfKPM/naAQBgPnozAAxOWusG3X2wqi5JcktVXZ3JBhR7k3xwyc0q\nyWlJTl1S91RVvSmTJvmNqvp/mTTgy7r7CwEA5qI3A8ARa4baJOnubya5dJXrn0hyzgrjjyW5eu7Z\nAQAr0psBYGKW048BAABgSxJqAQAAGC2hFgAAgNESagEAABgtoRYAAIDREmoBAAAYLaEWAACA0RJq\nAQAAGC2hFgAAgNESagEAABgtoRYAAIDREmoBAAAYLaEWAACA0RJqAQAAGC2hFgAAgNESagEAABgt\noRYAAIDREmoBAAAYLaEWAACA0RJqAQAAGC2hFgAAgNESagEAABgtoRYAAIDREmoBAAAYLaEWAACA\n0RJqAQAAGC2hFgAAgNESagEAABitmUJtVZ1fVXdW1T1VdX9V3VRVJ836S6rqpKq6r6p6/qkCAFN6\nMwBMrBlqq2pXkruSfLa7L07y+iRXJrl5Hb/nfUl+dK4ZAgDPoDcDwBGzvFN7fZJK8vEk6e5DmTTN\nd1bVmWsVV9Wrkrw1ye8uME8A4Ai9GQAGs4TaK5Ls7+7DS8b2JdmR5PLVCqvquUluS3JtkifnnSQA\n8Ax6MwAMZgm15yY5sGzs0eHyvDVqP5Dk8939tfVODAA4Kr0ZAAazbChxep79Su70eOfRiqrqwiRv\nSfK6WSdTVddm8spxzj777FnLAGC70ZsBYDDLO7WPJzl52dj0+NBKBVX1vAynNnX3zKc2dfee7t7d\n3bt37do1axkAbDd6MwAMZnmn9uEkyzedmB4/dJSaCzN5FfmjVTUde3mSVNXdw/Fbu/uvZ50oAPA0\nvRkABrOE2tuTXFdVO5ZsSHFRksNJ7lipoLu/muTHl45V1Y1J/l13v3Hu2QIAid4MAE+b5fTjW5J0\nkuuSpKpOS3JDklu7+8AwdkpVPVBVt23aTAGAKb0ZAAZrhtruPpjkkiRXVdU9mXxlwOeSvHvJzSrJ\naUlOXV5fVa8eTmu6Zji+u6p+e+GZA8A2pTcDwBGznH6c7v5mkktXuf6JJOcc5bpvJHnjPJMDAFam\nNwPAxCynHwMAAMCWJNQCAAAwWkItAAAAoyXUAgAAMFpCLQAAAKMl1AIAADBaQi0AAACjJdQCAAAw\nWkItAAAAoyXUAgAAMFpCLQAAAKMl1AIAADBaQi0AAACjJdQCAAAwWkItAAAAoyXUAgAAMFpCLQAA\nAKMl1AIAADBaQi0AAACjJdQCAAAwWkItAAAAoyXUAgAAMFpCLQAAAKMl1AIAADBaQi0AAACjJdQC\nAAAwWkItAAAAo3XSLDeqqvOT3JLk1CSnJdmb5IPd/dQqNRck+TdJLkhyOMkPJfl8kg93998sOG8A\n2Nb0ZgCYWDPUVtWuJHcl+VB3/05V7UyyL8nOJO9apfTGJJXkku7+u6p6cZI/TvITSd686MQBYLvS\nmwHgiFlOP74+kwb48STp7kNJbk7yzqo6c5W6/5Xkpu7+u6HuO0n+U5I3VdUZC80aALY3vRkABrOE\n2iuS7O/uw0vG9iXZkeTyoxV193u6+75lw08Ml89b1ywBgKX0ZgAYzBJqz01yYNnYo8Pleev8fW9I\n8uXufmSddQDAEXozAAxm2Sjq9CRPLhubHu+c9RdV1U9m8urxT61ym2uTXJskZ5999qx3DQDbjd4M\nAINZ3ql9PMnJy8amx4dm+SVV9SNJPpnkbd39p0e7XXfv6e7d3b17165ds9w1AGxHejMADGYJtQ8n\nWb7pxPT4obWKq+rlSW5P8uvdfed6JgcArEhvBoDBLKH29iS7q2rHkrGLMvl+uztWK6yqHxvqf6O7\n7xjGfqaqXjPnfAEAvRkAnjZLqL0lSSe5Lkmq6rQkNyS5tbsPDGOnVNUDVXXbtKiqzktydyZfFfA3\nVbW7qnYn+YVMvg8PAJiP3gwAgzU3iurug1V1SZJbqurqTDag2Jvkg0tuVklOS3LqkrFbk5yVyffm\nLbdv7hkDwDanNwPAEbPsfpzu/maSS1e5/okk5ywbO+r35AEAi9GbAWBiltOPAQAAYEsSagEAABgt\noRYAAIDREmoBAAAYLaEWAACA0RJqAQAAGC2hFgAAgNESagEAABgtoRYAAIDREmoBAAAYLaEWAACA\n0RJqAQAAGC2hFgAAgNESagEAABgtoRYAAIDREmoBAAAYLaEWAACA0RJqAQAAGC2hFgAAgNESagEA\nABgtoRYAAIDREmoBAAAYLaEWAACA0RJqAQAAGC2hFgAAgNESagEAABgtoRYAAIDREmoBAAAYrZlC\nbVWdX1V3VtU9VXV/Vd1UVSfNUPf8qvpEVd1XVfdW1aeq6iWLTxsAtje9GQAm1gy1VbUryV1JPtvd\nFyd5fZIrk9w8w/1/OsnOJLu7+7VJvpdkb1XtmH/KALC96c0AcMQs79Ren6SSfDxJuvtQJk3znVV1\n5tGKqurSJJck+XB39zD8oSQXJrlqkUkDwDanNwPAYJZQe0WS/d19eMnYviQ7kly+Rt2hJA9MB7r7\nkSSPDtcBAPPRmwFgMEuoPTfJgWVjjw6X561R99iSV4KX1q5WBwCsTm8GgMGaG0okOT3Jk8vGpsc7\n11k3rf2hlQqq6tok105vV1UPzjA/ju4fJvnu8Z7EyFnDxVnDjWEdF/ePjvcENpDePF7+lhdnDRdn\nDTeGdVzchvTmWULt40lOXjY2PT60zrpp7Yp13b0nyZ4kqar93b17hvlxFNZwcdZwcdZwY1jHxVXV\n/uM9hw2kN4+UNVycNVycNdwY1nFxG9WbZzn9+OEkyzedmB4/tEbdGVVVK9SuVgcArE5vBoDBLKH2\n9iS7l231f1GSw0nuWKNuZ5ILpgNVdVaSlyXZu/6pAgADvRkABrOE2luSdJLrkqSqTktyQ5Jbu/vA\nMHZKVT1QVbdNi7r7C5l8h977l7wi/IEk9yf5/Rl+756ZHwVHYw0XZw0XZw03hnVc3Im0hnrzeFnD\nxVnDxVnDjWEdF7cha1jP3gBxhRtVvTKTBnpKJq/w7k3ywe7+++H6U5M8mOSr3f2vltS9IMl/SPIT\nSX6Q5H8nub67/3IjJg8A25XeDAATM4VaAAAA2IpmOf14Q1XV+VV1Z1XdU1X3V9VNVbXmLsxV9fyq\n+kRV3VdV91bVp6rqJcdizlvNPGtYVRdU1X+sqj+uqi8NdR+tqhW/wuFEN+/zcEn9ScNzcVu/KrTI\nOlbVO4bn4l1V9WfD5QVrV55YFvhv4mVV9eWq2jdc3lFVrzkWc96qqupXquqvqurGGW+vrwz05sXp\nzYvTmzeG3rw4vXnjHKvefExDbVXtyuSzPJ/t7ouTvD7JlUlunqH805mcXrW7u1+b5HtJ9tYzN8k4\n4S2whjcm2ZXkku7+Z0n+RZKfT/LfNm+2W9OCz8Op9yX50U2Y3mgsso5V9VtJfjHJz3b3P0+yO8lL\nk7x80ya8Bc27hlV1bpI/TPIH3X1Rd78hk82B7hjuc1upqhdU1R8luTDJC9dRqq9Eb94IevPi9OaN\noTcvTm/eGMe8N3f3MftJ8qEk30myY8nYLyV5KsmZq9RdmsmGGP9kydjZw9gvHMvHcLx/FljDf5/k\nNcvGfnNYwzOO9+Mawxouue2rknwjyUcmf0LH/zGNaR2TnDPc5oJl4xckefHxflwjWcOrhr/dVy4Z\ne+Uw9vPH+3Edh3V8WZKfHv65k9w4Q42+cuRx683Hbw315gXXcMlt9eYF1lFv3pA11JufuR7HtDcf\n69OPr0iyv7sPLxnbl2RHksvXqDuU5IHpQHc/kuTR4brtZK417O73dPd9y4afGC6ft7FT3PLmfR6m\nqp6b5LYk1yZ5ctNmOA7zruPbkxzs7geXDnb3g939nY2f5pY27xp+KcmBJNdU1XOq6jlJrhmu+4vN\nmOhW1t3f7u6vrLNMXzlCb16c3rw4vXlj6M2L05s3wLHuzcc61J6byb/spR4dLs9bo+6xHuL6strV\n6k5E867hSt6Q5MvDk2U7WWQNP5Dk8939tQ2f1fjMu46vTvLtqvrlqvri8LmTz1TV6zZlllvbXGvY\n3d9NcnGSnx7qv53k+iQf6e6vbsI8T0T6yhF68+L05sXpzRtDb16c3nz8zN1XZv7w/QY5Pc9+BW16\nvHOdddPa7baZwrxr+AxV9ZOZvNr0Uxs0rzGZaw2r6sIkb0myHf8Dv5J5n4svyuQ0sSuTXNHd36+q\ndyXZV1Vv6O59Gz/VLWve5+KZSb6Y5L8neWMmX8vy5uH+mI2+coTevDi9eXF688bQmxenNx8/c/eV\nY/1O7eNJTl42Nj0+tM66ae1qdSeiedfwaVX1I0k+meRt3f2nGzi3sVj3GlbV8zKc2tTd2/3Upql5\nn4tPJXlukvd39/eTpLs/luRbSd6zwXPc6uZdw3cnOSPJe7v7qe7+QZLPJflYVf3qxk/zhKSvHKE3\nL05vXpzevDH05sXpzcfP3H3lWIfah5OcuWxsevzQGnVnVFWtULta3Ylo3jVMklTVy5PcnuTXu/vO\nDZ3ZeMyzhhdm8urRR6vq7qq6O8PnJKbHVbWend1OBPM+Fx9Zdjn159l+pyzOu4avSHKgu6efvcvw\n2Z9vZbKZBWvTV47QmxenNy9Ob94YevPi9ObjZ+6+cqxD7e1Jdi/bkvmiJIcz2fJ6tbqdmezAliSp\nqrMy2VVr7ybMcyubdw1TVT821P9Gd98xjP3MNvz+rHWvYXd/tbt/vLvfOP1J8p+H66Zjf73J895q\n5n0ufmG4/OFl4y9N8tjGTW8U5l3DR5K8eIXt7c9M8rcbO8UTlr5yhN68OL15cXrzxtCbF6c3Hz/z\n95Vjta3z8HnfXUn+Msm/Ho5PS3J/ko8tuc0pmex4dduy2i8m+VSSGo5/N8nXs2S77e3wM+8aZvIq\n2yNJbsjke8emP3uSXHO8H9cY1nCF+7kx2/trA+Z9Lp6Uydcu7Fny9/yWTD57cuXxflwjWcN/muTv\nk/zmkrFfy2TL+1883o/rOK/ps742QF/ZnOehNVx8DfXmjXkeLrsfvVlvPl5rqDcffU03vTcf03dq\nu/tgkkuSXFVV92SyPfbnMjkHfaoyefKcuqz8qiTfT7K/qu5N8vwkb+5nbrd9wltgDW9NclYmXxx9\n75KfdxyDaW8pCz4PU1WvXuEUp9/e5GlvOfOuY3c/leRNmbwS942q+p+Z/A/dZd39P47R9LeEBdbw\n60kuS3JFVX2lqr6S5LokV3f3J4/V/LeSqvq94e8ymXydwt1V9XPTq6OvHJXevDi9eXF688bQmxen\nN2+cY9mbpwkYAAAARudYf6YWAAAANoxQCwAAwGgJtQAAAIyWUAsAAMBoCbUAAACMllALAADAaAm1\nAAAAjJZQCwAAwGgJtQAAAIyWUAsAAMBoCbUAAACMllALAADAaAm1AAAAjJZQCwAAwGgJtQAAAIzW\nukJtVf1KVf1VVd044+2fX1WfqKr7qureqvpUVb1krpkCAM+iNwOw3c0UaqvqBVX1R0kuTPLCddz/\np5PsTLK7u1+b5HtJ9lbVjnXPFAB4mt4MABOzvlO7M8mN3X39rHdcVZcmuSTJh7u7h+EPZdJ8r1rX\nLAGA5fRmAMiMoba7v93dX1nnfV+R5FCSB5bczyNJHh2uAwDmpDcDwMRmbhR1bpLHlrwSPPVokvM2\n8fcCACvTmwE44Zy0ifd9epInVxh/MskPrVRQVdcmuTZJdu7c+ZpXvOIVmzc7ALaV++6777vdvet4\nz+M405sB2DI2qjdvZqh9PMnJK4yfnMmpT8/S3XuS7EmS3bt39/79+zdvdgBsK1X1f473HLYAvRmA\nLWOjevNmnn78cJIzqqqWjZ+Z5KFN/L0AwMr0ZgBOOJsZam/PZGfGC6YDVXVWkpcl2buJvxcAWJne\nDMAJZ0NCbVWdUlUPVNVt07Hu/kKSu5K8f8krwh9Icn+S39+I3wsArExvBmC7mDnUVtXvVdXdw+E1\nVXV3Vf3c9OokpyU5dVnZVUm+n2R/Vd2b5PlJ3tzdhxebNgCgNwPAOjaK6u63r3LdE0nOWWH8/ya5\nZq6ZAQCr0psBYHM/UwsAAACbSqgFAABgtIRaAAAARkuoBQAAYLSEWgAAAEZLqAUAAGC0hFoAAABG\nS6gFAABgtIRaAAAARkuoBQAAYLSEWgAAAEZLqAUAAGC0hFoAAABGS6gFAABgtIRaAAAARkuoBQAA\nYLSEWgAAAEZLqAUAAGC0hFoAAABGS6gFAABgtIRaAAAARkuoBQAAYLSEWgAAAEZLqAUAAGC0hFoA\nAABGS6gFAABgtIRaAAAARkuoBQAAYLRmCrVVdX5V3VlV91TV/VV1U1WdNEPdZVX15araN1zeUVWv\nWXzaALC96c0AMLFmqK2qXUnuSvLZ7r44yeuTXJnk5jXqzk3yh0n+oLsv6u43JLkjyR3DfQIAc9Cb\nAeCIWd6pvT5JJfl4knT3oUya5jur6sxV6l6V5HlJ9i4Z25vkhUkunmu2AECiNwPA02YJtVck2d/d\nh5eM7UuyI8nlq9R9KcmBJNdU1XOq6jlJrhmu+4s55goATOjNADBY87M3Sc5Ncv+ysUeHy/OOVtTd\n362qi5P810wa6A+SvCjJR7r7q3PMFQCY0JsBYDDLO7WnJ3ly2dj0eOfRiobTn76Y5L4kLxt+3prk\nT1apubaq9lfV/oMHD84wNQDYlvRmABjMEmofT3LysrHp8aFV6t6d5Iwk7+3up7r7B0k+l+RjVfWr\nKxV0957u3t3du3ftsl8FAByF3gwAg1lC7cNJlm86MT1+aJW6VyQ50N1PTAeGz/58K8kvrWOOAMAz\n6c0AMJgl1N6eZHdV7VgydlGSw5l8DcDRPJLkxcvqkknT/dt1zRIAWEpvBoDBLKH2liSd5LokqarT\nktyQ5NbuPjCMnVJVD1TVbUvq9iQ5Ncm/nQ5U1a8lOSvJf9mY6QPAtqQ3A8Bgzd2Pu/tgVV2S5Jaq\nujqTDSj2JvngkptVktMyaZTTuq9X1WVJbqyqfzkM70hydXd/cqMeAABsN3ozABwxy1f6pLu/meTS\nVa5/Isk5K4zfneSNc84NADgKvRkAJmY5/RgAAAC2JKEWAACA0RJqAQAAGC2hFgAAgNESagEAABgt\noRYAAIDREmoBAAAYLaEWAACA0RJqAQAAGC2hFgAAgNESagEAABgtoRYAAIDREmoBAAAYLaEWAACA\n0RJqAQAAGC2hFgAAgNESagEAABgtoRYAAIDREmoBAAAYLaEWAACA0RJqAQAAGC2hFgAAgNESagEA\nABgtoRYAAIDREmoBAAAYLaEWAACA0RJqAQAAGK2ZQm1VnV9Vd1bVPVV1f1XdVFUnzVj7jqr6UlXd\nVVV/NlxesNi0AWB705sBYGLN5ldVu5LcleRD3f07VbUzyb4kO5O8a43a30ry+iQ/293fq6p/kOTe\nJC9P8uCCcweAbUlvBoAjZnmn9vokleTjSdLdh5LcnOSdVXXm0Yqq6pwk70tyfXd/b6j9XpK3Jfna\ngvMGgO1MbwaAwSyh9ook+7v78JKxfUl2JLl8lbq3JznY3c941be7H+zu76x7pgDAlN4MAINZQu25\nSQ4sG3t0uDxvlbpXJ/l2Vf1yVX2xqvZV1Weq6nXzTBQAeJreDACDWULt6UmeXDY2Pd65St2Lkrwq\nyZVJrujui5J8Kcm+qrpopYKquraq9lfV/oMHD84wNQDYlvRmABjMEmofT3LysrHp8aFV6p5K8twk\n7+/u7ydJd38sybeSvGelgu7e0927u3v3rl27ZpgaAGxLejMADGYJtQ8nWb7pxPT4oVXqHll2OfXn\nWf3UKABgdXozAAxmCbW3J9ldVTuWjF2U5HCSO1ap+8Jw+cPLxl+a5LGZZwgALKc3A8BgllB7S5JO\ncl2SVNVpSW5Icmt3HxjGTqmqB6rqtiV1n0nyJ0neW1U13O4tSf5xJl87AADMR28GgMFJa92guw9W\n1SVJbqmqqzPZgGJvkg8uuVklOS3JqUvqnqqqN2XSJL9RVf8vkwZ8WXd/IQDAXPRmADhizVCbJN39\nzSSXrnL9E0nOWWH8sSRXzz07AGBFejMATMxy+jEAAABsSUItAAAAoyXUAgAAMFpCLQAAAKMl1AIA\nADBaQi0AAACjJdQCAAAwWkItAAAAoyXUAgAAMFpCLQAAAKMl1AIAADBaQi0AAACjJdQCAAAwWkIt\nAAAAoyXUAgAAMFpCLQAAAKMl1AIAADBaQi0AAACjJdQCAAAwWkItAAAAoyXUAgAAMFpCLQAAAKMl\n1AIAADDlBikgAAAOoElEQVRaQi0AAACjJdQCAAAwWkItAAAAoyXUAgAAMFpCLQAAAKM1U6itqvOr\n6s6quqeq7q+qm6rqpFl/SVWdVFX3VVXPP1UAYEpvBoCJNUNtVe1KcleSz3b3xUlen+TKJDev4/e8\nL8mPzjVDAOAZ9GYAOGKWd2qvT1JJPp4k3X0ok6b5zqo6c63iqnpVkrcm+d0F5gkAHKE3A8BgllB7\nRZL93X14ydi+JDuSXL5aYVU9N8ltSa5N8uS8kwQAnkFvBoDBLKH23CQHlo09Olyet0btB5J8vru/\ntt6JAQBHpTcDwGCWDSVOz7NfyZ0e7zxaUVVdmOQtSV4362Sq6tpMXjnO2WefPWsZAGw3ejMADGZ5\np/bxJCcvG5seH1qpoKqel+HUpu6e+dSm7t7T3bu7e/euXbtmLQOA7UZvBoDBLO/UPpxk+aYT0+OH\njlJzYSavIn+0qqZjL0+Sqrp7OH5rd//1rBMFAJ6mNwPAYJZQe3uS66pqx5INKS5KcjjJHSsVdPdX\nk/z40rGqujHJv+vuN849WwAg0ZsB4GmznH58S5JOcl2SVNVpSW5Icmt3HxjGTqmqB6rqtk2bKQAw\npTcDwGDNUNvdB5NckuSqqronk68M+FySdy+5WSU5Lcmpy+ur6tXDaU3XDMd3V9VvLzxzANim9GYA\nOGKW04/T3d9Mcukq1z+R5JyjXPeNJG+cZ3IAwMr0ZgCYmOX0YwAAANiShFoAAABGS6gFAABgtIRa\nAAAARkuoBQAAYLSEWgAAAEZLqAUAAGC0hFoAAABGS6gFAABgtIRaAAAARkuoBQAAYLSEWgAAAEZL\nqAUAAGC0hFoAAABGS6gFAABgtIRaAAAARkuoBQAAYLSEWgAAAEZLqAUAAGC0hFoAAABGS6gFAABg\ntIRaAAAARkuoBQAAYLSEWgAAAEZLqAUAAGC0hFoAAABGS6gFAABgtIRaAAAARuukWW5UVecnuSXJ\nqUlOS7I3yQe7+6lVai5I8m+SXJDkcJIfSvL5JB/u7r9ZcN4AsK3pzQAwsWaorapdSe5K8qHu/p2q\n2plkX5KdSd61SumNSSrJJd39d1X14iR/nOQnkrx50YkDwHalNwPAEbOcfnx9Jg3w40nS3YeS3Jzk\nnVV15ip1/yvJTd39d0Pdd5L8pyRvqqozFpo1AGxvejMADGYJtVck2d/dh5eM7UuyI8nlRyvq7vd0\n933Lhp8YLp+3rlkCAEvpzQAwmCXUnpvkwLKxR4fL89b5+96Q5Mvd/chKV1bVtVW1v6r2Hzx4cJ13\nDQDbht4MAINZQu3pSZ5cNjY93jnrL6qqn8zk1eNrj3ab7t7T3bu7e/euXbtmvWsA2G70ZgAYzBJq\nH09y8rKx6fGhWX5JVf1Ikk8meVt3/+ns0wMAVqA3A8BgllD7cJLlm05Mjx9aq7iqXp7k9iS/3t13\nrmdyAMCK9GYAGMwSam9PsruqdiwZuyiT77e7Y7XCqvqxof43uvuOYexnquo1c84XANCbAeBps4Ta\nW5J0kuuSpKpOS3JDklu7+8AwdkpVPVBVt02Lquq8JHdn8lUBf1NVu6tqd5JfyOT78ACA+ejNADA4\naa0bdPfBqrokyS1VdXUmG1DsTfLBJTerJKclOXXJ2K1Jzsrke/OW2zf3jAFgm9ObAeCINUNtknT3\nN5Ncusr1TyQ5Z9nYUb8nDwBYjN4MABOznH4MAAAAW5JQCwAAwGgJtQAAAIyWUAsAAMBoCbUAAACM\nllALAADAaAm1AAAAjJZQCwAAwGgJtQAAAIyWUAsAAMBoCbUAAACMllALAADAaAm1AAAAjJZQCwAA\nwGgJtQAAAIyWUAsAAMBoCbUAAACMllALAADAaAm1AAAAjJZQCwAAwGgJtQAAAIyWUAsAAMBoCbUA\nAACMllALAADAaAm1AAAAjJZQCwAAwGgJtQAAAIzWTKG2qs6vqjur6p6qur+qbqqqk2aoe35VfaKq\n7quqe6vqU1X1ksWnDQDbm94MABNrhtqq2pXkriSf7e6Lk7w+yZVJbp7h/j+dZGeS3d392iTfS7K3\nqnbMP2UA2N70ZgA4YpZ3aq9PUkk+niTdfSiTpvnOqjrzaEVVdWmSS5J8uLt7GP5QkguTXLXIpAFg\nm9ObAWAwS6i9Isn+7j68ZGxfkh1JLl+j7lCSB6YD3f1IkkeH6wCA+ejNADCYJdSem+TAsrFHh8vz\n1qh7bMkrwUtrV6sDAFanNwPAYM0NJZKcnuTJZWPT453rrJvW/tBKBVV1bZJrp7erqgdnmB9H9w+T\nfPd4T2LkrOHirOHGsI6L+0fHewIbSG8eL3/Li7OGi7OGG8M6Lm5DevMsofbxJCcvG5seH1pn3bR2\nxbru3pNkT5JU1f7u3j3D/DgKa7g4a7g4a7gxrOPiqmr/8Z7DBtKbR8oaLs4aLs4abgzruLiN6s2z\nnH78cJLlm05Mjx9ao+6MqqoValerAwBWpzcDwGCWUHt7kt3Ltvq/KMnhJHesUbczyQXTgao6K8nL\nkuxd/1QBgIHeDACDWULtLUk6yXVJUlWnJbkhya3dfWAYO6WqHqiq26ZF3f2FTL5D7/1LXhH+QJL7\nk/z+DL93z8yPgqOxhouzhouzhhvDOi7uRFpDvXm8rOHirOHirOHGsI6L25A1rGdvgLjCjapemUkD\nPSWTV3j3Jvlgd//9cP2pSR5M8tXu/ldL6l6Q5D8k+YkkP0jyv5Nc391/uRGTB4DtSm8GgImZQi0A\nAABsRbOcfgwAAABb0jEPtVV1flXdWVX3VNX9VXVTVa351UJV9fyq+kRV3VdV91bVp6rqJcdizlvN\nPGtYVRdU1X+sqj+uqi8NdR+tqhW/l/BEN+/zcEn9ScNzcVuf6rDIOlbVO4bn4l1V9WfD5QVrV55Y\nFvhv4mVV9eWq2jdc3lFVrzkWc96qqupXquqvqurGGW+vrwz05sXpzYvTmzeG3rw4vXnjHKvefExD\nbVXtymSDis9298VJXp/kyiQ3z1D+6Uw+M7S7u1+b5HtJ9tYzd3484S2whjcm2ZXkku7+Z0n+RZKf\nT/LfNm+2W9OCz8Op9yX50U2Y3mgsso5V9VtJfjHJz3b3P0+yO8lLk7x80ya8Bc27hlV1bpI/TPIH\n3X1Rd78hkx1v7xjuc1upqhdU1R8luTDJC9dRqq9Eb94IevPi9OaNoTcvTm/eGMe8N3f3MftJ8qEk\n30myY8nYLyV5KsmZq9Rdmskuj/9kydjZw9gvHMvHcLx/FljDf5/kNcvGfnNYwzOO9+Mawxouue2r\nknwjyUcmf0LH/zGNaR2TnDPc5oJl4xckefHxflwjWcOrhr/dVy4Ze+Uw9vPH+3Edh3V8WZKfHv65\nk9w4Q42+cuRx683Hbw315gXXcMlt9eYF1lFv3pA11JufuR7HtDcf69OPr0iyv7sPLxnbl2RHksvX\nqDuU5IHpQHc/kuTR4brtZK417O73dPd9y4afGC6ft7FT3PLmfR6mqp6b5LYk1yZ5ctNmOA7zruPb\nkxzs7geXDnb3g939nY2f5pY27xp+KcmBJNdU1XOq6jlJrhmu+4vNmOhW1t3f7u6vrLNMXzlCb16c\n3rw4vXlj6M2L05s3wLHuzcc61J6byb/spR4dLs9bo+6xHuL6strV6k5E867hSt6Q5MvDk2U7WWQN\nP5Dk8939tQ2f1fjMu46vTvLtqvrlqvri8LmTz1TV6zZlllvbXGvY3d9NcnGSnx7qv53k+iQf6e6v\nbsI8T0T6yhF68+L05sXpzRtDb16c3nz8zN1XZv7w/QY5Pc9+BW16vHOdddPa7baZwrxr+AxV9ZOZ\nvNr0Uxs0rzGZaw2r6sIkb0myHf8Dv5J5n4svyuQ0sSuTXNHd36+qdyXZV1Vv6O59Gz/VLWve5+KZ\nSb6Y5L8neWMm3zX65uH+mI2+coTevDi9eXF688bQmxenNx8/c/eVY/1O7eNJTl42Nj0+tM66ae1q\ndSeiedfwaVX1I0k+meRt3f2nGzi3sVj3GlbV8zKc2tTd2/3Upql5n4tPJXlukvd39/eTpLs/luRb\nSd6zwXPc6uZdw3cnOSPJe7v7qe7+QZLPJflYVf3qxk/zhKSvHKE3L05vXpzevDH05sXpzcfP3H3l\nWIfah5OcuWxsevzQGnVnVFWtULta3Ylo3jVMklTVy5PcnuTXu/vODZ3ZeMyzhhdm8urRR6vq7qq6\nO8PnJKbHVbWend1OBPM+Fx9Zdjn159l+pyzOu4avSHKgu6efvcvw2Z9vZbKZBWvTV47QmxenNy9O\nb94YevPi9ObjZ+6+cqxD7e1Jdi/bkvmiJIcz2fJ6tbqdmezAliSpqrMy2VVr7ybMcyubdw1TVT82\n1P9Gd98xjP3MNvz+rHWvYXd/tbt/vLvfOP1J8p+H66Zjf73J895q5n0ufmG4/OFl4y9N8tjGTW8U\n5l3DR5K8eIXt7c9M8rcbO8UTlr5yhN68OL15cXrzxtCbF6c3Hz/z95Vjta3z8HnfXUn+Msm/Ho5P\nS3J/ko8tuc0pmex4dduy2i8m+VSSGo5/N8nXs2S77e3wM+8aZvIq2yNJbsjke8emP3uSXHO8H9cY\n1nCF+7kx2/trA+Z9Lp6Uydcu7Fny9/yWTD57cuXxflwjWcN/muTvk/zmkrFfy2TL+1883o/rOK/p\ns742QF/ZnOehNVx8DfXmjXkeLrsfvVlvPl5rqDcffU03vTcf03dqu/tgkkuSXFVV92SyPfbnMjkH\nfaoyefKcuqz8qiTfT7K/qu5N8vwkb+5nbrd9wltgDW9NclYmXxx975KfdxyDaW8pCz4PU1WvXuEU\np9/e5GlvOfOuY3c/leRNmbwS942q+p+Z/A/dZd39P47R9LeEBdbw60kuS3JFVX2lqr6S5LokV3f3\nJ4/V/LeSqvq94e8ymXydwt1V9XPTq6OvHJXevDi9eXF688bQmxenN2+cY9mbpwkYAAAARudYf6YW\nAAAANoxQCwAAwGgJtQAAAIyWUAsAAMBoCbUAAACMllALAADAaAm1AAAAjJZQCwAAwGj9//nEv6pj\nfxPTAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7fa0fe11f5c0>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "\"\"\"Generate a pub-quality precip figure\"\"\"\n",
    "\n",
    "# set the dictionary keys over which we will iterate\n",
    "models = ['PERSIANN','ne16np4','ne30np4','ne120np4']\n",
    "# set the color of map lines (semi-transparent black)\n",
    "mapLineColor = [0,0,0,0.5]\n",
    "# set the labels of the plots\n",
    "panel_labels = ['(a)','(b)','(c)','(d)']\n",
    "\n",
    "\n",
    "# initialize the plot with a 2x2 subpanel\n",
    "fig,axs = PP.subplots(2,2,figsize=(16,8))\n",
    "\n",
    "# create a mapping between linear axis index and subpanel location\n",
    "ax_grid = [(0,0),(0,1),(1,0),(1,1)]\n",
    "\n",
    "# loop over the models\n",
    "for i,key in enumerate(models):\n",
    "    # select the current axis on which to plot\n",
    "    ax = axs[ax_grid[i]]\n",
    "    \n",
    "    # initialize the map using the 'robinson' projection, whcih truncates at the poles\n",
    "    # center the longitude at the mid-point of the map\n",
    "    center_lon = 0.5*(lon[0]+lon[-1])\n",
    "    projection = cartopy.crs.Robinson(center_lon)\n",
    "\n",
    "    \n",
    "    #Draw the plot\n",
    "    if key == 'PERSIANN':\n",
    "        # draw a contour plot of the PERSIANN data with 64 levels and the 'Set1_r' colormap\n",
    "        cplot = ax.contourf(xlon,xlat,precip[key],64,transform=projection,cmap='Set1_r')\n",
    "    else:\n",
    "        # draw a contour plot of the given model, using the contour levels and colormap from the PERSIANN plot\n",
    "        m.contourf(xlon,xlat,precip[key],levels=cplot.levels,transform=projection,cmap=cplot.cmap)\n",
    "        \n",
    "    # draw coast lines with a slightly thick line\n",
    "    m.drawcoastlines(color=mapLineColor,linewidth=1.5)\n",
    "    \n",
    "    # draw the bounding boxes in light gray with thick/semi-transparent lines\n",
    "    # note the use of a list comprehension here instead of a for-loop\n",
    "    #[ax.add_patch(mpl.patches.Polygon(bboxToPolygon(bbox,m),edgecolor='gray',facecolor='none',linewidth=3,alpha=0.1)) for bbox in bboxes]\n",
    "\n",
    "    \n",
    "    # add the panel label\n",
    "    ax.text(0.09, 0.01, panel_labels[i], \\\n",
    "            horizontalalignment='center', \\\n",
    "            verticalalignment='center', \\\n",
    "            transform=ax.transAxes, \\\n",
    "            weight='bold', \\\n",
    "            size=15)\n",
    "\n",
    "# adjust spacing between panels and make room for the colorbar\n",
    "fig.subplots_adjust(right=0.92,wspace=-0.05,hspace=0.05)\n",
    "\n",
    "# add an axis for the master colorbar\n",
    "cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])\n",
    "\n",
    "# draw the colorbar\n",
    "cb = fig.colorbar(cplot, cax=cbar_ax)\n",
    "\n",
    "# set the colorbar label, orientation, and font\n",
    "cb.set_label('Precipitation Rate [mm day$^{-1}$]',rotation=270,weight='bold',size=15,labelpad=25)\n",
    "\n",
    "# set the font of the colorbar labels \n",
    "tickprop = mpl.font_manager.FontProperties(family='sans-serif',weight='bold',size=15)\n",
    "for label in cb.ax.get_yticklabels():\n",
    "    label.set_fontproperties(tickprop)\n",
    "\n",
    "# save as a jpeg image with a resolution of 300 dots per inch\n",
    "PP.savefig('fig_iliad.jpg',dpi=300)\n",
    "\n",
    "PP.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import cartopy\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on module cartopy.crs in cartopy:\n",
      "\n",
      "NAME\n",
      "    cartopy.crs\n",
      "\n",
      "FILE\n",
      "    /home/taobrien/anaconda3/lib/python3.6/site-packages/cartopy/crs.py\n",
      "\n",
      "\n"
     ]
    }
   ],
   "source": [
    "help(cartopy.crs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
