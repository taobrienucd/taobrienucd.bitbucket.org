import simpleMPI # import a simple wrapper around the MPI library
import glob # import the glob library for getting lists of files
import netCDF4 as nc # import the netcdf library for reading netcdf files
from numpy import * # import numpy

# define a function for calculating the average temperature within a file
def calc_avg_temp(infilename):
    """Given an ERSST file, calculate the global average ocean temperature and return the average"""

    with nc.Dataset(infilename,'r') as fin:
        # read the sea surface temperature variable
        sst = fin.variables['sst'][0,0,:,:]
        # read the latitude variable for weighting the average
        lat = fin.variables['sst'][:]

    # make the latitude variable have the same ~shape as sst
    lat2d = lat[:,newaxis]

    # take the cosine of the latitude
    lat_wgt = cos(pi*lat2d/180.)

    # calculate the weighted average
    sst_avg = sum(sst*lat_wgt)/sum(lat_wgt)

    return sst_avg

# Initialize MPI 
smpi = simpleMPI.simpleMPI()

# have only the first process get the list of files
if smpi.rank == 0:
    # get a list of files on which to operate
    file_list = glob.glob("/projects/data/ERSST/*.nc")

    # print the number of files and the name of the first file
    smpi.pprint("{} files, e.g., {}".format(len(file_list), file_list[0]))
else:
    # have all other processes create an empty list
    file_list = []

# Scatter the list to all processors (my_list differs among processes now)
my_list = smpi.scatterList(file_list)

# Print the length of the lists (as well as the rank of the printing process; smpi.pprint() takes care of this)
smpi.pprint("Working on {} files".format(len(my_list)))

# run the SST avg function on all of the files (for the given process)
sst_avg = []
for nfile in my_list:
    # calculate the average SST and add it to the list
    sst_avg.append(calc_avg_temp(nfile))

sst_avg = array(sst_avg)

# once all processes have completed, use the gather function to gather all of the averages back to a single process 
# (process 0 [root=0])
all_sst_avgs = smpi.comm.gather(sst_avg,root=0)

# have process 0 calculate the average of the averages
if smpi.rank == 0:
    full_average = average(concatenate(all_sst_avgs))
    smpi.pprint("Average SST: {:2.2f} C".format(full_average))
