cimport cython # provides @cython.boundscheck()
cimport numpy as np # import C versions of numpy routines
import numpy as np # import numpy routines

# note the addition of the 'cpdef list' and 'long [:]' parts in this line.  These tell Cython
# the expected type of the return variable and the type of the input variable (`long [:]` means an array of integers)
@cython.boundscheck(False) # turn of array bounds checking; this speeds up loops (use with caution!)
cpdef list cy_rain_event_lengths(long [:] qpcp_inches):
    """Given a vector of 'rain record', determines the length of all non-zero events
    
        input:
        ------
        qpcp_inches : a vector where 0 represents a non-event and any positive number represents a rain event
        
    
    """
    # loop over the precipitation variable
    cdef int number_of_events = 0

    # initialize the event counter to 0
    cdef int num_events = 0

    # inialize the 'in_precip' flag
    cdef int in_precip = False

    # initialize the event lengths list
    cdef list event_lengths = []

    # initialize the current event length
    cdef int current_length = 0

    # define the event counter
    cdef int i
    
    # define the is_precip flag
    cdef int is_precip
    
    # loop over indices of the qpcp_inches variable
    for i in range(len(qpcp_inches)):

        # determine if this is a precipitation event
        if qpcp_inches[i] > 0:
            is_precip = True
        else:
            # flag that this isn't a precip event
            is_precip = False

        # determine if we are already in a precipitation event
        if is_precip:
            # check if we are in a precip event
            if in_precip:
                # increment the current event length counter
                current_length = current_length + 1

            else:
                # flag that we are now in a precipitation event
                in_precip = True

                # increment the event counter
                num_events = num_events + 1

                # set the current event length counter to 1
                current_length = 1
        else:
            # check if we were in a precip event
            if in_precip:
                # if so, the current event has ended; add it to the list
                event_lengths.append(current_length)

            # if this isn't a precip event, flag that we aren't in a precip
            # event
            in_precip = False
            
    return event_lengths
