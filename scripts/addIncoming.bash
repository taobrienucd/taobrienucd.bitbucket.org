#!/bin/bash

# Get the noteblog base directory
export SCRIPT_PATH=$(dirname "$0")

python ${SCRIPT_PATH}/addPost.py --clobber --remove --commit --rebuild ${SCRIPT_PATH}/../incoming_notebooks/*
