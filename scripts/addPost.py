#!/usr/bin/env python
import os
import git
import datetime as dt
import os, errno
import shutil
import subprocess
import sys

#Prepend the ipython-3.0.0 path so that we use a version that
#can understand nbformat v4
#sys.path.insert(0,"/usr/local/ipython-3.0.0/lib/python2.7/site-packages")
import IPython
try:
    import nbformat
except:
    try:
        import IPython.nbformat as nbformat
    except:
        pass

def mkdir_p(path):
    """mkdir -p function"""
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def addImage(   fileName, \
                overwrite=False, \
                commit=False, \
                rebuild=False, \
                putFileInScratch=False):
    """Adds an image to the content/images directory"""

    #Get the repo directory
    repoDir = "/".join(os.path.dirname(os.path.abspath(__file__)).split('/')[:-1])
    baseDir = repoDir + "/content"
    imageDir = baseDir + "/images"

    #Check for clobbering
    if os.path.exists(fileName) and (not overwrite):
        raise RuntimeError,"{} exists and overwrite is True".format(fileName)

    #Ensure the image directory exists
    mkdir_p(imageDir)
    
    #Copy the file
    shutil.copy2(fileName,imageDir)

    #Deal with committing the newly added file
    if commit:
        repo = git.Repo(repoDir)
        #Add the post to the git repo
        repo.git.add(fileName)
        message = "Add/update {}".format(os.path.basename(fileName))
        #Commit the changes
        repo.git.commit(m = message)

    #Rebuild the site
    if rebuild:
        os.chdir(repoDir)
        print subprocess.check_output('make html'.split())

    #Put the original file(s) in the local scratch
    #directory
    if putFileInScratch:
        #Move the original file to /.../scratch
        scratchDir = "{}/scratch".format(repoDir)
        mkdir_p(scratchDir)
        scratchFile = "{}/{}".format(scratchDir,os.path.basename(fileName))
        shutil.move(fileName,scratchFile)
    return 

class postFile:
    """A class for holding information about a file to add as a blog post"""

    def __init__(self):
        self.title = None
        self.creationDate = None
        self.content = None
        self.copyToNotebooksDir = False
        self.copyName = None
        self.origFile = None
        self.origNotebookFile = None

    def write(self,overwrite=False,commit=False,rebuild=False,putFileInScratch=False):
        """Writes the post to the blog's content directory"""

        #Determine the base directory (assumes this file resides in
        #the /.../scripts directory)
        repoDir = "/".join(os.path.dirname(os.path.abspath(__file__)).split('/')[:-1])
        baseDir = repoDir + "/content"

        #Set the file's directory
        fileDir = "{base}/{year}/{month:02}".format( \
                    base = baseDir, \
                    year = self.creationDate.year, \
                    month = self.creationDate.month)

        #Make the directory if needed
        mkdir_p(fileDir)

        #Set the file name
        baseName = "{year}-{month}-{day}-{title}.md".format( \
                year = self.creationDate.year, \
                month = self.creationDate.month, \
                day = self.creationDate.day, \
                title = '_'.join(self.title.split()))
        fileName = "{}/{}".format(fileDir,baseName)

        #Check for clobbering
        if os.path.exists(fileName) and (not overwrite):
            raise RuntimeError,"{} exists and overwrite is True".format(fileName)

        #Write the file
        with open(fileName,'w') as fout:
            fout.write(self.content)

        if self.copyToNotebooksDir:
            #Set the notebook base directory
            notebookBaseDir = '{}/downloads/notebooks'.format(baseDir)
            mkdir_p(notebookBaseDir)
            #Set the path to which to copy
            copyFile = '{}/{}'.format(notebookBaseDir,self.copyName)
            #Copy the file
            #shutil.copy(self.origNotebookFile,copyFile)
            #Force-write the file as nb version 3
            try:
                noteBook = IPython.nbformat.read(self.origNotebookFile,as_version=3)
            except:
                noteBook = nbformat.read(self.origNotebookFile,as_version=3)

            try:
                IPython.nbformat.write(noteBook,copyFile)
            except:
                nbformat.write(noteBook,copyFile)
            

        #Deal with committing the newly added files
        if commit:
            repo = git.Repo(repoDir)
            #Add the post to the git repo
            repo.git.add(fileName)
            message = "Add/update {}".format(os.path.basename(fileName))
            #If there is an underlying notebook, add the notebook
            if self.copyToNotebooksDir:
                repo.git.add(copyFile)
                message += " and {}".format(os.path.basename(copyFile))

            #Commit the changes
            repo.git.commit(m = message)

        #Rebuild the site
        if rebuild:
            os.chdir(repoDir)
            print subprocess.check_output('make html'.split())

        #Put the original file(s) in the local scratch
        #directory
        if putFileInScratch:
            #Move the original file to /.../scratch
            scratchDir = "{}/scratch".format(repoDir)
            mkdir_p(scratchDir)
            scratchFile = "{}/{}".format(scratchDir,os.path.basename(self.origFile))
            shutil.move(self.origFile,scratchFile)
            

class wrappedFile(postFile):
    """A class for parsing information from a postable file"""

    def __init__(self, \
                 fileName):
        """Given a filename, parse the file needed for adding a blog post"""

        #Initialize the super class
        #super(wrappedFile,self).__init__()
        postFile.__init__(self)

        #Save the original file
        self.origFile = fileName

        #Get the file creation date
        self.creationDate = dt.datetime.fromtimestamp(os.path.getmtime(fileName))

        #Set the title as the file's name (without the extension)
        self.title = '.'.join(os.path.basename(fileName).split('.')[:-1])
        #Also get the file's extension
        fileExtension = os.path.basename(fileName).split('.')[-1]


        #***********************************************
        # Set the file's content based on the extension
        #***********************************************
        #*********
        # Markdown
        #*********
        if fileExtension == "md":
            #Read the file's content
            with open(fileName,'r') as fin:
                self.baseContent = fin.read()

        #******************
        # IPython Notebook
        #******************
        elif fileExtension == "ipynb":
            self.origNotebookFile = fileName
            self.copyName = '_'.join(os.path.basename(fileName).split())
            self.baseContent = "category:notebooks\n{{% notebook {pfile} %}}".format(\
                                pfile = self.copyName)
            self.copyToNotebooksDir = True

        # Unhandled types (raise exception)
        else:
            raise ValueError,"Unknown filetype for {}".format(fileName)

        #Set the file's content
        self.content = \
"""title: {title}
date: {date}
{content}
            """.format( title = self.title, \
                        date = ":".join(self.creationDate.isoformat(' ').split(':')[:-1]), \
                        content = self.baseContent)


#*******************************************************************************
#*******************************************************************************
#********************** Parse command line arguments ***************************
#*******************************************************************************
#*******************************************************************************
if __name__ == "__main__":
    import argparse
    import glob

    parser = argparse.ArgumentParser()

    parser.add_argument('--clobber', \
                      help="Clobber any existing files",default=True,action='store_true')
    parser.add_argument('--rebuild', \
                      help="Rebuild the site",default=True,action='store_true')
    parser.add_argument('--commit', \
                      help="Commit any files",default=True,action='store_true')
    parser.add_argument('--remove', \
                      help="Remove any files to /.../scratch",default=True,action='store_true')
    parser.add_argument('--quiet', \
                      help="Don't be verbose",default=False,action='store_true')
    parser.add_argument('inputfile',nargs='*',help="Input file names")

#python /projects/NoteBlog/scripts/addPost.py --clobber --remove --commit --rebuild /var/spool/noteblog/*

    parsedArgs = vars(parser.parse_args())
    putFileInScratch = parsedArgs['remove']
    doClobber = parsedArgs['clobber']
    rebuild = parsedArgs['rebuild']
    commit = parsedArgs['commit']
    inFileNames = parsedArgs['inputfile'][:]
    beQuiet = parsedArgs['quiet']

    #Check if we need to do an expansion on the files
    if len(inFileNames) == 1 and ('*' in inFileNames[0]):
        inFileNames = sorted(glob.glob(inFileNames[0]))

    #Check if there were any file names provided
    numFiles = len(inFileNames)
    if numFiles == 0:
        if not beQuiet:
            print "Error: No files were provided\n"
            parser.print_help()
        quit()

    for i in xrange(numFiles):
        fileName = inFileNames[i]
        #Only rebuild on the last file
        if i == numFiles - 1:
            doRebuild = rebuild
        else:
            doRebuild = False

        #Get the file's extension
        extension = fileName.split('.')[-1]
        #Wrap any md or ipynb files
        if extension == "md" or extension == "ipynb":
            #Wrap all files provided on the command line
            wrappedFile(fileName).write(overwrite=doClobber, \
                                        commit=commit, \
                                        rebuild=doRebuild, \
                                        putFileInScratch=putFileInScratch)
        #Assume that anything else is an image
        else:
            addImage(   fileName, \
                        overwrite=doClobber, \
                        commit=commit, \
                        rebuild=doRebuild, \
                        putFileInScratch=putFileInScratch)


